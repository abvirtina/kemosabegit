<?php
/**
 * Plugin Name: WooCommerce Multi-Step Checkout Pro
 * // /* Plugin URI: https://www.silkypress.com/wp-multi-step-checkout-pro/ 
 * Plugin URI: http://locahost/silkypress/wp-multi-step-checkout-pro/ 
 * Description: Nice multi-step checkout for your WooCommerce store
 * Version: 2.6
 * Author: SilkyPress
 * Author URI: https://www.silkypress.com
 * License: GPL2
 *
 * Text Domain: wp-multi-step-checkout-pro
 * Domain Path: /languages/
 *
 * WC requires at least: 2.3.0
 * WC tested up to: 3.7
 * Requires PHP: 5.2.4
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'WPMultiStepCheckoutPro' ) ) :
/**
 * Main WPMultiStepCheckoutPro Class
 *
 * @class WPMultiStepCheckoutPro
 */
final class WPMultiStepCheckoutPro {
    public $version = '2.6';
    public $options = array();

    protected static $_instance = null;

    public $theme = '';

    /**
     * Main WPMultiStepCheckoutPro Instance
     *
     * Ensures only one instance of WPMultiStepCheckoutPro is loaded or can be loaded
     *
     * @static
     * @return WPMultiStepCheckoutPro - Main instance
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
      * Cloning is forbidden.
      */
    public function __clone() {
         _doing_it_wrong( __FUNCTION__, __( 'An error has occurred. Please reload the page and try again.' ), '1.0' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, __( 'An error has occurred. Please reload the page and try again.' ), '1.0' );
    }

    /**
     * WPMultiStepCheckout Constructor
     */
    public function __construct() {

        define('WMSC_PLUGIN_PRO_FILE', __FILE__);
        define('WMSC_PLUGIN_PRO_URL', plugins_url('/', __FILE__));
        define('WMSC_PLUGIN_PRO_PATH', plugin_dir_url('/', __FILE__));
        define('WMSC_PRO_VERSION', $this->version );
        define('WMSC_PLUGIN_PRO_NAME', 'WooCommerce Multi-Step Checkout Pro');
        define('WMSC_PLUGIN_PRO_SERVER', 'https://www.silkypress.com');
        define('WMSC_PLUGIN_PRO_AUTHOR', 'Diana Burduja');

        if ( ! class_exists('woocommerce') ) {
          add_action( 'admin_notices', array($this, 'install_woocommerce_admin_notice' ) );
          return false;
        }

        if ( is_admin() ) {
            include_once 'includes/admin-side.php';
        }

        // Replace the checkout template
        add_filter( 'woocommerce_locate_template', array( $this, 'woocommerce_locate_template' ), 10, 3 );


        require_once 'includes/settings-array.php';
        $this->options = get_option('wmsc_options', array());
        $default_settings = get_wmsc_settings('wp-multi-step-checkout-pro');
        foreach($default_settings as $_key => $_value ) $default_settings[$_key] = $_value['value'];
        $this->options = array_merge( $default_settings, $this->options );

        $this->adjust_hooks();

        // Enqueue the scripts for the frontend
        add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
        add_action( 'wp_head', array( $this, 'wp_head') );
        add_action( 'wp_head', array( $this, 'compatibilities'), 40 );
        add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
        add_action( 'init', array($this, 'load_plugin_textdomain' ) ); 

    }

    /**
     * Modify the default WooCommerce hooks
     */
    public function adjust_hooks() {
      // Remove login messages
      remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
      remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

      // Split the `Order` and the `Payment` tabs
      remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
      remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
      add_action( 'wpmc-woocommerce_order_review', 'woocommerce_order_review', 20 );
      add_action( 'wpmc-woocommerce_checkout_payment', 'woocommerce_checkout_payment', 10 );

      // Split the `woocommerce_before_checkout_form`
      remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
      remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
      add_action( 'wpmc-woocommerce_checkout_login_form', 'woocommerce_checkout_login_form', 10 );
      add_action( 'wpmc-woocommerce_checkout_coupon_form', 'woocommerce_checkout_coupon_form', 10 );

      // Add the content functions to the steps
      add_action( 'wmsc_step_content_login', 'wmsc_step_content_login', 10);
      add_action( 'wmsc_step_content_shipping', 'wmsc_step_content_shipping', 10);
      add_action( 'wmsc_step_content_billing', 'wmsc_step_content_billing', 10);

      // Add the content functions to the Payment and Order Review steps
      if ( class_exists('WooCommerce_Germanized') ) {
          add_action( 'wmsc_step_content_review', 'wmsc_step_content_review_germanized', 10);
          add_action( 'wmsc_step_content_payment', 'wmsc_step_content_payment_germanized', 10);
          add_action( 'wpmc-woocommerce_order_review', 'woocommerce_gzd_template_render_checkout_checkboxes', 10 );
          add_filter( 'wc_gzd_checkout_params', array($this, 'wc_gzd_checkout_params' ));
      } elseif ( class_exists('Woocommerce_German_Market') ) {
          add_action( 'wmsc_step_content_review', 'wmsc_step_content_review_german_market', 10);
          add_action( 'wmsc_step_content_payment', 'wmsc_step_content_payment', 10);
      } else {
          add_action( 'wmsc_step_content_review', 'wmsc_step_content_review', 10);
          add_action( 'wmsc_step_content_payment', 'wmsc_step_content_payment', 10);
      }

      // Add product thumbnail in the Order Review section
      if (isset($this->options['review_thumbnails']) && $this->options['review_thumbnails'] == 1 && !$this->theme('avada')) { 
          add_filter('woocommerce_cart_item_name', array($this, 'cart_thumbnail'), 20, 3);
          add_filter('woocommerce_checkout_cart_item_quantity', array($this, 'cart_thumbnail_end'), 20, 3); 
      }

      // Compatibility with the Shopper theme
      add_filter('shopper_sticky_order_review', '__return_false');
    }

    /**
     * Load the form-checkout.php template from this plugin
     */
    public function woocommerce_locate_template( $template, $template_name, $template_path ){
        if( 'checkout/form-checkout.php' != $template_name )
          return $template;

        $template = plugin_dir_path( __FILE__ ) . 'includes/form-checkout.php';
        return $template;
    }

    /**
     * Enqueue the JS and CSS assets
     */
    public function wp_enqueue_scripts() {
        $template = (isset($this->options['template'])) ? $this->options['template'] : 'default';

        $u = plugins_url('/', __FILE__) . 'assets/'; // URL of the assets folder
        $v = $this->version; // this plugin's version
        $d = array( 'jquery' ); // dependencies
        $w = false; // where? in footer?
        $p = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

        // Localization variables
        $vars = array(
            'keyboard_nav'          => (isset($this->options['keyboard_nav']) && $this->options['keyboard_nav'] ) ? true : false, 
            'clickable_steps'       => (isset($this->options['clickable_steps']) && $this->options['clickable_steps'] ) ? true : false, 
            'validation_per_step'   => (isset($this->options['validation_per_step']) && $this->options['validation_per_step'] ) ? true : false, 
            'error_msg'             => (isset($this->options['t_error']) && $this->options['t_error']) ? __($this->options['t_error']) : '',
        );
        if ( isset($this->options['t_wpml']) && $this->options['t_wpml'] == 1 ) {
            $defaults = get_wmsc_settings('wp-multi-step-checkout-pro');
            $vars['error_msg'] = $defaults['t_error']['value'];
        }

        // Load scripts
        wp_register_script( 'wpmc', $u . 'js/script'.$p.'.js', $d, $v, $w );
        wp_localize_script( 'wpmc', 'WPMC', $vars );
        wp_register_style ( 'wpmc', $u.'css/style-'.$template.$p.'.css',  array(), $v );
        if ( is_checkout() ) {
            wp_enqueue_script ( 'wpmc' );
            wp_enqueue_style  ( 'wpmc' ); 
        }
    }


    /**
     * Change the main color
     */
    public function wp_head() {
        if ( ! is_checkout() ) return; 
        $color = (isset($this->options['main_color'])) ? $this->options['main_color'] : '#1e85be';
        $visited_color = (isset($this->options['visited_color'])) ? $this->options['visited_color'] : '#1EBE3A';
        $check_sign = (isset($this->options['wpmc_check_sign']) && $this->options['wpmc_check_sign'] ) ? true : false;
      ?>
      <style type="text/css">
      .wpmc-tabs-wrapper .wpmc-tab-item.visited::before { border-bottom-color: <?php echo $visited_color; ?> }
      .wpmc-tabs-wrapper .wpmc-tab-item.visited .wpmc-tab-number { border-color: <?php echo $visited_color; ?> }
      .wpmc-tabs-wrapper-md .wpmc-tab-item.visited .wpmc-tab-number { background-color: <?php echo $visited_color; ?> }
      .wpmc-tabs-wrapper-breadcrumb .wpmc-tab-item.visited .wpmc-tab-number {background-color: <?php echo $visited_color; ?>}
      .wpmc-tabs-wrapper .wpmc-tab-item.current::before {border-bottom-color: <?php echo $color; ?>}
      .wpmc-tabs-wrapper .wpmc-tab-item.current .wpmc-tab-number {border-color: <?php echo $color; ?>}
      .wpmc-tabs-wrapper-breadcrumb {border-color: <?php echo $color; ?> !important;} 
      .wpmc-tabs-wrapper-breadcrumb .wpmc-tabs-list .wpmc-tab-item:after {box-shadow: 3px -3px 0 1px <?php echo $color; ?>}
      .wpmc-tabs-wrapper-breadcrumb .wpmc-tab-item.current .wpmc-tab-number {background-color: <?php echo $color; ?>}
      .wpmc-tabs-wrapper-md .wpmc-tab-item.current .wpmc-tab-number {background-color: <?php echo $color; ?>}
      @media screen and (max-width: 767px) {
      .wpmc-tabs-wrapper-breadcrumb .wpmc-tabs-list.wpmc-5-tabs .wpmc-tab-item.current {border-left: 3px solid <?php echo $color; ?>}
      }
      .woocommerce-checkout-review-order-table img.attachment-woocommerce_thumbnail { margin:0 auto; margin-bottom: 0; max-width:6em;height:auto}
      .wpmc-button, .wpmc-button:hover { padding: 10px 18px;background-color: <?php echo $color; ?>;color: white; border: none;font-size: 100%; }
      .wpmc-button::after, .wpmc-button::before, .wpmc-button:hover { content: none !important; padding: 10px 18px; border: none; color: white; font-size: 100%;}
      .woocommerce-checkout form.login .form-row { width: 100%; float: none; }
      .woocommerce-checkout .col2-set { width: 100%; }
      <?php if ( $check_sign ) { ?>
      .wpmc-tabs-wrapper .wpmc-tab-item .wpmc-tab-number::before {
      content: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path style="fill:%23ffffff" d="M9.95 26.2c-0.1 0-0.15 0-0.25 0-0.45-0.050-0.9-0.35-1.1-0.8l-5.45-10.55c-0.4-0.75-0.1-1.65 0.65-2 0.75-0.4 1.65-0.1 2 0.65l4.5 8.75 16.1-15.95c0.6-0.6 1.55-0.6 2.1 0 0.6 0.6 0.6 1.55 0 2.1l-17.5 17.35c-0.25 0.3-0.65 0.45-1.050 0.45z"></path></svg>') !important; 
        width: 14px;
        height: 14px;
        position: absolute;
        opacity: 0;
        top: 3px;
        left: 6px;
       }
      .wpmc-tabs-wrapper-default .wpmc-tab-item .wpmc-tab-number::before {
      content: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path style="fill:<?php echo urlencode($visited_color); ?>" d="M9.95 26.2c-0.1 0-0.15 0-0.25 0-0.45-0.050-0.9-0.35-1.1-0.8l-5.45-10.55c-0.4-0.75-0.1-1.65 0.65-2 0.75-0.4 1.65-0.1 2 0.65l4.5 8.75 16.1-15.95c0.6-0.6 1.55-0.6 2.1 0 0.6 0.6 0.6 1.55 0 2.1l-17.5 17.35c-0.25 0.3-0.65 0.45-1.050 0.45z"></path></svg>') !important; 
        left: 8px;
        top: 2px;
      }
      .wpmc-tabs-wrapper .wpmc-tab-item.visited .wpmc-tab-number { color: transparent; position: relative; }
      .wpmc-tabs-wrapper .wpmc-tab-item.current .wpmc-tab-number { color: white; }
      .wpmc-tabs-wrapper-default .wpmc-tab-item.current .wpmc-tab-number { color: <?php echo $color; ?>; }
      .wpmc-tabs-wrapper .wpmc-tab-item.visited .wpmc-tab-number::before { opacity: 1;}
      .wpmc-tabs-wrapper .wpmc-tab-item.current .wpmc-tab-number::before { opacity: 0;}
      <?php }
       
      if (isset($this->options['review_thumbnails']) && $this->options['review_thumbnails'] == 1 ) { ?>
      .woocommerce_cart_item_name {display: flex;width: 100%;flex-direction: row;align-items: center;}
      .woocommerce_cart_item_name_title {flex: auto; padding-left: 20px;}
      table.woocommerce-checkout-review-order-table .product-name { width: 60%; }
      .woocommerce-checkout-review-order-table .product-total { vertical-align: middle; }
      <?php } 
        
      if ( is_rtl() ) { ?>
        .wpmc-tabs-list .wpmc-tab-item {float: right;}
      <?php }

        ?>
      </style>
      <?php
    }


    /**
     * Compatibilities with themes
     */
    public function compatibilities() {
      if ( ! is_checkout() ) return; 

      if ( $this->theme('storefront')) { ?>
        <style type="text/css">
          #order_review, #order_review_heading { float: left; width: 100%; }
        </style>
      <?php }

      if ( $this->theme('avada')) { ?>
        <style type="text/css">
          .wpmc-nav-wrapper { float: left; margin-top: 10px; }
          .woocommerce-checkout a.continue-checkout{display: none;}
          .woocommerce-error,.woocommerce-info,.woocommerce-message{padding:1em 2em 1em 3.5em;margin:0 0 2em;position:relative;background-color:#f7f6f7;color:#515151;border-top:3px solid #a46497;list-style:none outside;width:auto;word-wrap:break-word}.woocommerce-error::after,.woocommerce-error::before,.woocommerce-info::after,.woocommerce-info::before,.woocommerce-message::after,.woocommerce-message::before{content:' ';display:table}.woocommerce-error::after,.woocommerce-info::after,.woocommerce-message::after{clear:both}.woocommerce-error .button,.woocommerce-info .button,.woocommerce-message .button{float:right}.woocommerce-error li,.woocommerce-info li,.woocommerce-message li{list-style:none outside!important;padding-left:0!important;margin-left:0!important}.rtl.woocommerce .price_label,.rtl.woocommerce .price_label span{direction:ltr;unicode-bidi:embed}.woocommerce-message{border-top-color:#8fae1b}.woocommerce-info{border-top-color:#1e85be}.woocommerce-info::before{color:#1e85be}.woocommerce-error{border-top-color:#b81c23}.woocommerce-checkout .shop_table td, .woocommerce-checkout .shop_table th {padding: 10px}.woocommerce .single_add_to_cart_button, .woocommerce button.button {margin-top: 10px}
          .woocommerce .woocommerce-form-coupon-toggle { display: none; }
          .woocommerce .checkout_coupon { display: flex !important; }
        </style>
      <?php }


        if ( $this->theme('theretailer')) { ?>
          <style type="text/css">
          .wpmc-nav-buttons button.button { display: none !important; }
          .wpmc-nav-buttons button.button.current { display: inline-block !important; }
          </style>
        <?php }

      if ( $this->theme('Divi')) { ?>
        <style type="text/css">
            #wpmc-back-to-cart:after, #wpmc-prev:after { display: none; } 
            #wpmc-back-to-cart:before, #wpmc-prev:before{ position: absolute; left: 1em; margin-left: 0em; opacity: 0; font-family: "ETmodules"; font-size: 32px; line-height: 1em; content: "\34"; -webkit-transition: all 0.2s; -moz-transition: all 0.2s; transition: all 0.2s; } 
            #wpmc-back-to-cart:hover, #wpmc-prev:hover { padding-right: 0.7em; padding-left: 2em; left: 0.15em; } 
            #wpmc-back-to-cart:hover:before, #wpmc-prev:hover:before { left: 0.2em; opacity: 1;}
        </style>
      <?php }

       if ( $this->theme('enfold')) { ?>
        <style type="text/css">
            .wpmc-footer-right { width: auto; }
        </style>
       <?php }

      if ( $this->theme('flatsome')) { ?>
        <style type="text/css">
            .processing::before, .loading-spin { content: none; }
            .wpmc-footer-right button.button { margin-right: 0; }
        </style>
       <?php }

       if ( $this->theme('bridge')) { ?>
        <style type="text/css">
            .woocommerce input[type="text"]:not(.qode_search_field), .woocommerce input[type="password"], .woocommerce input[type="email"], .woocommerce textarea, .woocommerce-page  input[type="tel"], .woocommerce-page input[type="text"]:not(.qode_search_field), .woocommerce-page input[type="password"], .woocommerce-page input[type="email"], .woocommerce-page   textarea, .woocommerce-page select { width: 100%; }
            .woocommerce-checkout table.shop_table { width: 100% !important; }
        </style>
       <?php }

       if ( $this->theme('zass')) { ?>
        <style type="text/css">form.checkout.woocommerce-checkout.processing:after {content: '';}.woocommerce form.checkout.woocommerce-checkout.processing:before {display: none;}</style>
       <?php }

       if ( $this->theme('shopper')) { ?>
        <style type="text/css">#order_review_heading, #order_review { width: 100%; float: none; }</style>
       <?php }


       if ( defined( 'WPB_VC_VERSION' ) ) { ?>
        <style type="text/css">
            .woocommerce-checkout .wpb_column .vc_column-inner::after{clear:none !important; content: none !important;}
            .woocommerce-checkout .wpb_column .vc_column-inner::before{content: none !important;}
        </style>
        <?php } 

       if ( class_exists('WooCommerce_Germanized') ) { ?>
         <style type="text/css"> #order_review_heading {display: block !important;} h3#order_payment_heading { display: none !important; } .wc-gzd-product-name-left { display: none; } </style>
        <?php
           remove_action( 'woocommerce_review_order_after_payment', 'woocommerce_gzd_template_render_checkout_checkboxes', 10 );
      }
    }


    /**
     * Show product thumbnails in the Order Review section
     */
    function cart_thumbnail( $product_title, $cart_item, $cart_item_key ) {

        if ( !is_checkout()) return $product_title;

        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

        echo '<div class="woocommerce_cart_item_name"><div class="woocommerce_cart_item_name_thumbnail">';
        echo wp_kses_post( $thumbnail );
        echo '</div><div class="woocommerce_cart_item_name_title">';
        echo $product_title; 
    }
    function cart_thumbnail_end( $quantity, $cart_item, $cart_item_key) {
        if ( !is_checkout()) return;
        echo $quantity . '</div></div>';
    } 


    /**
     * Compatibilities with themes
     */
    public function after_setup_theme() {
        if ( $this->theme('Avada') ) { 
            if ( function_exists('avada_woocommerce_before_checkout_form' ) ) {
                remove_action( 'woocommerce_before_checkout_form', 'avada_woocommerce_before_checkout_form' );
            }

            if ( function_exists( 'avada_woocommerce_checkout_after_customer_details' ) ) {
                remove_action( 'woocommerce_checkout_after_customer_details', 'avada_woocommerce_checkout_after_customer_details' );
            }

            if ( function_exists( 'avada_woocommerce_checkout_before_customer_details' ) ) {
                remove_action( 'woocommerce_checkout_before_customer_details', 'avada_woocommerce_checkout_before_customer_details' );
            }
            global $avada_woocommerce;

            if( ! empty( $avada_woocommerce ) ){
                remove_action( 'woocommerce_before_checkout_form', array( $avada_woocommerce, 'avada_top_user_container' ), 1 );
                remove_action( 'woocommerce_before_checkout_form', array( $avada_woocommerce, 'checkout_coupon_form' ), 10 );
                remove_action( 'woocommerce_before_checkout_form', array( $avada_woocommerce, 'before_checkout_form' )  );
                remove_action( 'woocommerce_after_checkout_form',  array( $avada_woocommerce, 'after_checkout_form' ) );
            }
        }

        if ( $this->theme('hestia-pro') ) {
            remove_action( 'woocommerce_before_checkout_form', 'hestia_coupon_after_order_table_js' );
        }
    }


    function wc_gzd_checkout_params($params) {
        $params['adjust_heading'] = false;
        return $params;
    }


    /**
     * Is $string theme active?
     */
    public function theme($string) {
        $string = strtolower($string);
        if (empty($this->theme)) {
            $this->theme = strtolower(get_template());
        }
        if (strpos($this->theme, $string ) !== false)
            return true;

        return false;
    }


    /**
     * Admin notice that WooCommerce is not activated
     */
    public function install_woocommerce_admin_notice() {
      ?>
      <div class="error">
          <p><?php _e( 'The WP Multi-Step Checkout plugin is enabled, but it requires WooCommerce in order to work.', 'Alert Message: WooCommerce require', 'wp-multi-step-checkout-pro' ); ?></p>
      </div>
      <?php
    }


    /**
     * Load the textdomain
     */
	public function load_plugin_textdomain() {
        load_plugin_textdomain( 'wp-multi-step-checkout-pro', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
	}

}

endif;

/**
 * Returns the main instance of WPMultiStepCheckoutPro
 *
 * @return WPMultiStepCheckoutPro
 */
function WPMultiStepCheckoutPro() {
    return WPMultiStepCheckoutPro::instance();
}

WPMultiStepCheckoutPro();

function wpmc_pro_plugin_settings_link($links) {
    $action_links = array(
        'settings' => '<a href="' . admin_url( 'admin.php?page=wmsc-settings' ) . '" aria-label="' . esc_attr__( 'View plugin\'s settings', 'wp-multi-step-checkout-pro' ) . '">' .       esc_html__(  'Settings', 'wp-multi-step-checkout-pro' ) . '</a>',
    );
    return array_merge( $action_links, $links );
}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'wpmc_pro_plugin_settings_link');
