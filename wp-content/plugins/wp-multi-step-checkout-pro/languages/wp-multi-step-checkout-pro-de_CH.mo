��            )   �      �     �     �  #   �     �     �  !     �   8     �  
   �     �     �     �  A   �     A     J     S  )   \  J   �      �  G   �  #   :  
   ^  
   i  [   t  #   �  "   �  
        "  6   =  �  t          .     7     M     T     \  �   p     9	     ?	     N	  
   U	     `	  ^   h	     �	     �	     �	  2   �	  O   
  (   h
  Q   �
  )   �
  
          b   ,  #   �  6   �     �     �  6                              
                	                                                                                              Back to cart Billing Frontend: button labelBack to cart Frontend: button labelNext Frontend: button labelPrevious Frontend: button labelSkip Login If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing &amp; Shipping section. Login Main Color Next Order Payment Please fix the errors on this step before moving to the next step Previous Settings Shipping Show the <code>Back to Cart</code> button Show the <code>Billing</code> and the <code>Shipping</code> steps together Show the <code>Login</code> step Show the <code>Order</code> and the <code>Payment</code> steps together Show the <code>Shipping</code> step SilkyPress Skip Login The WP Multi-Step Checkout plugin is enabled, but it requires WooCommerce in order to work. WooCommerce Multi-Step Checkout Pro You must be logged in to checkout. Your order https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ Project-Id-Version: WooCommerce Multi-Step Checkout 1.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-multi-step-checkout-pro
PO-Revision-Date: 2018-12-12 21:13+0100
Last-Translator: Diana Burduja <diana@wootips.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
Plural-Forms: nplurals=2; plural=(n != 1);
 Zurück zum Warenkorb Rechnung Zurück zum Warenkorb Weiter Zurück Login überspringen Wenn du schon einmal bei uns bestellt hast, gib bitte deinen Benutzernamen bzw. E-Mail und dein Passwort ein. Falls du das erste mal bestellst, fülle bitte die Zahlungs- und Versandangaben unten aus. Login Die Hauptfarbe Weiter Bestellung Zahlung Bitte beheben Sie die Fehler in diesem Schritt, bevor Sie mit dem nächsten Schritt fortfahren Zurück Einstellungen Versand <code>Zurück zum Warenkorb</code> Button anzeigen Die Schritte <code>Rechnung</code> und <code>Versand</code> kombiniert anzeigen Den Schritt <code>Login
</code> anzeigen Die Schritte <code>Bestellung</code> und <code>Zahlung</code> kombiniert anzeigen Den Schritt <code>Versand</code> anzeigen SilkyPress Login überspringen Das WP Multi-Step Checkout Plugin is aktiviert, aber es benötigt WooCommerce um zu funktionieren. WooCommerce Multi-Step Checkout Pro Bitte melde dich an, um die Bestellung abzuschließen. Deine Bestellung https://www.silkypress.com https://www.silkypress.com/wp-multi-step-checkout-pro/ 