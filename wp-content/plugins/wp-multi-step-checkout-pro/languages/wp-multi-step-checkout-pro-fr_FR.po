# Translation of Plugins - WooCommerce Multi-Step Checkout - Development (trunk) in French (France)
# This file is distributed under the same license as the Plugins - WooCommerce Multi-Step Checkout - Development (trunk) package.
msgid ""
msgstr ""
"Project-Id-Version: Plugins - WooCommerce Multi-Step Checkout - Development "
"(trunk)\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-multi-step-"
"checkout-pro\n"
"POT-Creation-Date: 2018-12-12 18:09:26+00:00\n"
"PO-Revision-Date: 2018-12-12 20:06+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.2\n"

#: includes/form-checkout.php:72
msgid ""
"If you have shopped with us before, please enter your details in the boxes "
"below. If you are a new customer, please proceed to the Billing &amp; "
"Shipping section."
msgstr ""
"Si vous avez déjà commandé avec nous auparavant, veuillez saisir vos "
"coordonnées ci-dessous. Si vous êtes un nouveau client, veuillez renseigner "
"la section facturation et livraison."

#: includes/form-checkout.php:82
msgid "You must be logged in to checkout."
msgstr "Vous devez être identifié pour commander."

#: includes/form-checkout.php:114
msgid "Your order"
msgstr "Votre commande"

#: includes/form-checkout.php:120 includes/settings-array.php:153
#: includes/settings-array.php:155
msgid "Payment"
msgstr "Paiement"

#: includes/settings-array.php:11
msgid "Which Steps to show"
msgstr ""

#: includes/settings-array.php:17
msgid "Show the <code>Shipping</code> step"
msgstr "Afficher l'étape d'Expédition"

#: includes/settings-array.php:23
msgid "Show the <code>Login</code> step"
msgstr ""

#: includes/settings-array.php:29
msgid ""
"Show the <code>Billing</code> and the <code>Shipping</code> steps together"
msgstr "Afficher ensemble les étapes de Facturation et d'Expédition"

#: includes/settings-array.php:35
msgid "Show the <code>Order</code> and the <code>Payment</code> steps together"
msgstr "Afficher ensemble la Commande et les étapes de Paiement"

#: includes/settings-array.php:42
msgid "Functionality"
msgstr ""

#: includes/settings-array.php:48
msgid "Validate the fields during each step"
msgstr ""

#: includes/settings-array.php:49
msgid ""
"The default WooCommerce validation is done when clicking the Place Order "
"button. With this option the validation is performed when trying to move to "
"the next step"
msgstr ""

#: includes/settings-array.php:56
msgid "Clickable Steps"
msgstr ""

#: includes/settings-array.php:57
msgid "The user can click on the steps in order to get to the next one."
msgstr ""

#: includes/settings-array.php:64
msgid "Enable the keyboard navigation"
msgstr ""

#: includes/settings-array.php:65
msgid ""
"Use the keyboard's left and right keys to move between the checkout steps"
msgstr ""

#: includes/settings-array.php:72
msgid "Additional Elements"
msgstr ""

#: includes/settings-array.php:78
msgid "Show the <code>Back to Cart</code> button"
msgstr "Afficher le bouton \"Retour au panier\""

#: includes/settings-array.php:84
msgid "Show registration form in the Login step"
msgstr ""

#: includes/settings-array.php:88
msgid ""
"The registration form will be shown next to the login form, it will not "
"replace it"
msgstr ""

#: includes/settings-array.php:92
msgid "Add product thumbnails to the Order Review section"
msgstr ""

#: includes/settings-array.php:101
msgid "Main Color"
msgstr "Couleur principale"

#: includes/settings-array.php:107
msgid "Template"
msgstr ""

#: includes/settings-array.php:109
msgid "default"
msgstr ""

#: includes/settings-array.php:111
msgid "Default"
msgstr ""

#: includes/settings-array.php:112
msgid "Material Design"
msgstr ""

#: includes/settings-array.php:113
msgid "Breadcrumbs"
msgstr ""

#: includes/settings-array.php:119
msgid "Use the plugin's buttons"
msgstr ""

#: includes/settings-array.php:122
msgid ""
"By default the plugin tries to use the theme's design for the buttons. If "
"this fails, enable this option in order to use the plugin's button style"
msgstr ""

#: includes/settings-array.php:129 includes/settings-array.php:131
msgid "Login"
msgstr "Connexion"

#: includes/settings-array.php:135 includes/settings-array.php:137
msgid "Billing"
msgstr "Facturation"

#: includes/settings-array.php:141 includes/settings-array.php:143
msgid "Shipping"
msgstr "Livraison"

#: includes/settings-array.php:147 includes/settings-array.php:149
msgid "Order"
msgstr "Commande"

#: includes/settings-array.php:159
msgid "Back to cart"
msgstr "Retour au panier"

#: includes/settings-array.php:161
msgctxt "Frontend: button label"
msgid "Back to cart"
msgstr "Retour au panier"

#: includes/settings-array.php:165
msgid "Skip Login"
msgstr "Ignorer l'Identification"

#: includes/settings-array.php:167
msgctxt "Frontend: button label"
msgid "Skip Login"
msgstr "Ignorer l'Identification"

#: includes/settings-array.php:171
msgid "Previous"
msgstr "Précédent"

#: includes/settings-array.php:173
msgctxt "Frontend: button label"
msgid "Previous"
msgstr "Précédent"

#: includes/settings-array.php:177
msgid "Next"
msgstr "Suivant"

#: includes/settings-array.php:179
msgctxt "Frontend: button label"
msgid "Next"
msgstr "Suivant"

#: includes/settings-array.php:183 includes/settings-array.php:185
msgid "Please fix the errors on this step before moving to the next step"
msgstr ""

#: includes/settings-array.php:187
msgid "This is an error message shown in the frontend"
msgstr ""

#: includes/settings-array.php:191
msgid "Use WPML to translate the text on the Steps and Buttons"
msgstr ""

#: includes/settings-array.php:195
msgid ""
"For a multilingual website the translations from WPML will be used instead "
"of the ones in this form"
msgstr ""

#: wp-multi-step-checkout-pro.php:58 wp-multi-step-checkout-pro.php:65
msgid "An error has occurred. Please reload the page and try again."
msgstr ""

#: wp-multi-step-checkout-pro.php:325
msgid ""
"The WP Multi-Step Checkout plugin is enabled, but it requires WooCommerce in "
"order to work."
msgstr ""
"Le plugin WP Multi-Step Checkout est activé, mais il nécessite WooCommerce "
"pour fonctionner."

#: wp-multi-step-checkout-pro.php:355
msgid "Settings"
msgstr "Réglages"

#. Plugin Name of the plugin/theme
msgid "WooCommerce Multi-Step Checkout Pro"
msgstr "WooCommerce Multi-Step Checkout Pro"

#. Plugin URI of the plugin/theme
msgid "https://www.silkypress.com/wp-multi-step-checkout-pro/"
msgstr "https://www.silkypress.com/wp-multi-step-checkout-pro/"

#. Description of the plugin/theme
msgid "Nice multi-step checkout for your WooCommerce store"
msgstr "Agréable multi-step checkout pour votre boutique WooCommerce"

#. Author of the plugin/theme
msgid "SilkyPress"
msgstr "SilkyPress"

#. Author URI of the plugin/theme
msgid "https://www.silkypress.com"
msgstr "https://www.silkypress.com"

#~ msgid "Cheatin&#8217; huh?"
#~ msgstr "Cheatin&#8217; huh?"

#~ msgid "Order & Payment"
#~ msgstr "Commande & Paiement"

#~ msgid "Billing & Shipping"
#~ msgstr "Facturation & Expédition"
