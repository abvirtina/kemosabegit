jQuery(document).ready(function( $ ){
    $('[data-toggle="tooltip"]').tooltip();

    // When changing the color, adjust the color's hex next to the color field
    $('input[type=color]').on('change', function() {
        $(this).siblings('span').text($(this).val());
    });

    // Enable/disable text fields when WPML is toggled
    toggle_wpml();
    $('#t_wpml').change( toggle_wpml );
    function toggle_wpml() {
        var all_text = '#t_login, #t_billing, #t_shipping, #t_order, #t_payment, #t_back_to_cart, #t_skip_login, #t_previous, #t_next, #t_error'; 
        if ($('#t_wpml').is(':checked') ) {
            $(all_text).prop('disabled', true);
        } else {
            $(all_text).prop('disabled', false);
        }
    }

    // Design preview
    change_preview_template();
    $('input[name=template]').change( change_preview_template );
    function change_preview_template() {
        var template = $('input[name=template]:checked').val();

        // Change stylesheet
        if ( typeof $('#wmsc-style-css').attr('href') !== 'undefined' ) {
            var stylesheet = $('#wmsc-style-css').attr('href').replace(/style-(.*?)\.css/g, 'style-'+template+'.css');
            $('#wmsc-style-css').attr('href', stylesheet);
        }

        // Change .wpmc-wrapper class
        $('.wpmc-preview .wpmc-tabs-wrapper')
            .removeClass('wpmc-tabs-wrapper-md')
            .removeClass('wpmc-tabs-wrapper-default')
            .removeClass('wpmc-tabs-wrapper-breadcrumb')
            .addClass('wpmc-tabs-wrapper-'+template);
    }
    change_preview_color();
    $('#main_color').change( change_preview_color );
    function change_preview_color() {
        var color = $('#main_color').val();
        var css_text = $('#wpmc-preview-css').text();

        if ( css_text.length ) {
            var old_css_color = css_text.match(/: #(.*?)\}/)[1];
            $('#wpmc-preview-css').text( css_text.replace(new RegExp('#'+old_css_color, 'g'), color));
        }
    }

    // Arrange the settings form
    $('label[for="registration_with_login"]').parent().css('margin-bottom', '0');
    $('label[for="registration_with_login"]').css({'padding-bottom': '0', 'height' : '30px'});

});
