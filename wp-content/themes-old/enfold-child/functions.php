<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

/**
 * Google Tag Manager Body Script
 */
function google_tag_manager_body_code() { ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTPPXBF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) —>

<?php }
add_action( 'wp_footer', 'google_tag_manager_body_code', 1);

/**
 * Google Tag Manager Head Script
 */
function google_tag_manager_head_code(){ ?>
   
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTPPXBF');</script>


<!-- End Google Tag Manager -->

<?php }
add_action('wp_head', 'google_tag_manager_head_code', 1);

add_filter('avia_post_nav_settings', 'avia_post_nav_settings_mod');
function avia_post_nav_settings_mod($settings)
{
  if(is_singular('product')) {
        $settings['taxonomy'] = 'product_cat';
	$settings['is_fullwidth'] = false;
  }
  $settings['same_category'] = true;
  return $settings;
}
add_filter('avf_form_from', 'avf_form_from_mod', 10, 3);
function avf_form_from_mod($from, $new_post, $form_params) {
    $from = " woodrow@kemosabe.com ";
    return $from;
}

add_filter( 'woocommerce_product_tabs', 'kemosabe_remove_product_tabs', 98 );
 
function kemosabe_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}